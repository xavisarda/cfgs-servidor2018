CREATE TABLE `season` (
  `idseason` int(11) NOT NULL AUTO_INCREMENT,
  `idserie` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `nchapters` int(11) DEFAULT NULL,
  `season` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idseason`),
  KEY `FK_serie` (`idserie`),
  CONSTRAINT `FK_serie` FOREIGN KEY (`idserie`) REFERENCES `serie` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE season 
    ADD CONSTRAINT FK_serie 
    FOREIGN KEY (idserie) 
    REFERENCES serie(sid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
    
CREATE VIEW seriesdb.seriesnseasons 
	AS SELECT sid, title, serie.year, major, count(idseason) AS nseasons 
	FROM serie 
	LEFT JOIN season 
	ON sid = idserie 
	GROUP BY sid, title, serie.year, major;
	
ALTER TABLE serie
	DROP COLUMN nseasons;
	

