<?php 

require_once(__DIR__.'/../model/db/SerieDb.php');

class SeriesController{
    
    public function getSerie($id){
        $db = new SerieDb();
        return $db->getSerie($id);
    }
    
    public function getSeries($random = true){
        $db = new SerieDb();
        $llistaoriginal = $db->getSeries();
        
        /*
        if($random){
            return $this->randomizeList($llistaoriginal);
        }
        */
        
        return $llistaoriginal;
    }
    
    public function createSerie($t, $y, $m){
        $db = new SerieDb();
        return $db->insertSerie($t, $y, $m);
    }
    
    public function deleteSerie($id){
        $db = new SerieDb();
        return $db->deleteSerie($id);
    }
    
    public function updateSerie($t, $y, $m, $i){
        $db = new SerieDb();
        return $db->updateSerie($t, $y, $m, $i);
    }
    
    private function randomizeList($llistaoriginal){
        $llista = array();
        $reps = count($llistaoriginal);
        for($i = 0 ; $i < $reps ; $i++){
            $indexr = rand(0, count($llistaoriginal)-1);
            array_push($llista, $llistaoriginal[$indexr]);
            $newlist = array();
            for($ii = 0 ; $ii < count($llistaoriginal) ; $ii++){
                if($ii != $indexr){
                    array_push($newlist, $llistaoriginal[$ii]);
                }
                $llistaoriginal = $newlist;
            }
        }
    }
}