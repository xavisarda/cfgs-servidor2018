<?php

    require_once(__DIR__.'/classes/Persona.php');
  $active = 0;
  //No apareix a l'html
  //$a = array();
  //array_push($a, 'Hola', 'Fuet', 3, 'Vermell', '3.1416');

  $a = [ 'Hola', 'Fuet', 3, 'Groc', '2.713'];

  $b = [ "Andorra" => "Andorra la vella", "Portugal" => "Lisboa",
          "Japón" => "Tokyo" ];

?><html>
  <head>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <title>Primer ejemplo de PHP</title>
  </head>
  <body>
    <!-- Això sí que apareix -->
    <div id="wrapper">
        <?php require_once(__DIR__.'/modules/header.php'); ?>
        <h1>Ejemplo de PHP</h1>
        <p>Spicy jalapeno bacon ipsum dolor amet ham hock flank prosciutto swine, pork loin kevin andouille. Pork loin meatball prosciutto, tail corned beef salami kevin cow burgdoggen leberkas short ribs beef pancetta drumstick picanha. Tongue leberkas fatback turducken ball tip, ham hock chuck. Shankle hamburger jowl ribeye biltong pork loin, swine drumstick. Pork chop pork belly pork shankle.</p>
        <ul>
          <?php for($i = 0 ; $i < count($a) ; $i++ ){ ?>
            <li><?php echo($a[$i]); ?></li>
          <?php } ?>
        </ul>
        <h2>Paises y sus capitales</h2>
        <ol>
          <?php foreach ($b as $k => $v) { ?>
            <li><strong><?=$k?></strong>-<?=$v?></li>
          <?php } ?>
        </ol>
        <?php $p = new Persona("Foo", "Fooo", 18); ?>
        <table>
        	<tr><th>Name</th><th>Lastname</th><th>Age</th></tr>
        	<tr>
        		<td><?=$p->getName()?></td>
        		<td><?=$p->getLastname()?></td>
        		<td><?=$p->getAge()?></td>
    		</tr>
        </table>
    </div>
  </body>
</html>
