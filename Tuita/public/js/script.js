
//Load login form on index page
function loadform(){
	$('#login-form').load('/html/loginform');
}

//load tuits from the api
function loadTuitsHome(){
	$.ajax({
		"url":"/tuits",
		"method":"get",
		"accept":"application/json"
	}).done(function(response){
		var htmltuits = '';
		$.each(response, function(index, value){
			htmltuits += '<article>';
			htmltuits += '<p>'+value.msg+'</p>';
			htmltuits += '<h3>'+value.usr+'</h3>';
			htmltuits += '<div>'+value.fav+'</div>';
			htmltuits += '</article>';
		});
		$('#hp-tuits').html(htmltuits);
	});
}

//send new msg
function sendmsg(){
	var newtuitmsg = $('#hp-newmsg').val();
	var newtuitusr = 'profeA';
	var newtuit = { "msg" : newtuitmsg };
	
	$.ajax({
		url:"/user/"+newtuitusr+"/tuit",
		method:"post",
		accept:"application/json",
		data : newtuit,
		dataType: "application/json"
	}).done(function(response){
		$('#hp-newmsg').val('');
		loadTuitsHome();
	});
}