<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require(__DIR__.'/../vendor/autoload.php');
require_once(__DIR__.'/../lib/controller/UserController.php');
require_once(__DIR__.'/../lib/controller/TuitaController.php');

$app = new \Slim\App;

$app->get('/tuits',
    function(Request $request, Response $response, array $args) {
        $cnt = new TuitaController();
        $retcnt = $cnt->getTuits();
        
        $newarray = array();
        
        foreach ($retcnt as $tuit){
            array_push($newarray, $tuit->toArray());
        }
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($newarray));
        
        return $newR;
        
    });

$app->post('/user',
    function(Request $request, Response $response, array $args) {
        $userjson = $request->getParsedBody();
        
        $cnt = new UserController();
        $retcnt = $cnt->createUser($userjson['username']);
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($retcnt));
        
        return $newR;
        
    });

$app->get('/user/{username}',
    function(Request $request, Response $response, array $args) {
        $uname = $args['username'];
        
        $cnt = new UserController();
        $retcnt = $cnt->getUser($uname);
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($retcnt));
        
        return $newR;
        
    });

$app->delete('/user/{username}',
    function(Request $request, Response $response, array $args) {
        $uname = $args['username'];
        
        $cnt = new UserController();
        $retcnt = $cnt->removeUser($uname);
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($retcnt));
        
        return $newR;
        
    });

$app->post('/user/{username}/tuit',
    function(Request $request, Response $response, array $args) {
        $uname = $args['username'];
        $msgjson = $request->getParsedBody();
     
        $tcnt = new TuitaController();
        $tuit = $tcnt->postTuit($msgjson['msg'],$uname);
        
        $ucnt = new UserController();
        $user = $ucnt->addTuit2User($tuit);
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($tuit));
        
        return $newR;
        
    });

$app->put('/user/{username}/tuit/{tuid}/fav',
    function(Request $request, Response $response, array $args) {
        $uname = $args['username'];
        $tuid = $args['tuid'];
        
        //sumar 1 a fav
        $tcnt = new TuitaController();
        $tuit = $tcnt->fav($tuid);
        
        $ucnt = new UserController();
        $user = $ucnt->fav($uname, $tuid);
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($tuit));
        
        return $newR;
    });

//HTML Views

$app->get('/html/loginform',
    function(Request $request, Response $response, array $args) {
        //$form = '<form>';
        $form = '<label for="un">Message</label>';
        $form .= '<input type="text" id="hp-newmsg"/>';
        $form .= '<button id="hp-newmsgB">Login</button>';
        //$form .= '</form>';
        $newR = $response->withHeader('Content-type', 'text/html');
        $newR->getBody()->write(($form));
        
        return $newR;
    });

$app->run();