<?php

require_once(__DIR__.'/../controller/IndexController.php');

$symbol = $_GET['s'];

$cnt = new IndexController();
$currency = $cnt->getCurrency($symbol);

?><html>
	<head>
		<title>Update Currency</title>
	</head>
	<body>
		<div id="wrapper">
			<h1>Update currency</h1>
			<div id="form">
				<form method="post" action="/forms/update.php">
					<dl>
						<dt><label for="cname">Name</label></dt>
						<dd><input type="text" id="cname" name="cname" value="<?=$currency->getName()?>" /></dd>
						<dt><label for="csymb">Symbol</label></dt>
						<dd><input type="text" id="csymb" name="csymb" value="<?=$currency->getSymbol()?>"/></dd>
						<dt><label for="cev">Euro value</label></dt>
						<dd><input type="numeric" id="cev" name="cev" value="<?=$currency->getEurval()?>" /></dd>
						<dt><label for="ctype">Type</label></dt>
						<dd>
							<select id="ctype" name="ctype">
								<option value="material" <?php if($currency->getType() == "material"){ echo "selected"; }?>>Material</option>
								<option value="crypto" <?php if($currency->getType() == "crypto"){ echo "selected"; }?>>Cryptocurrency</option>
							</select>
						</dd>
						<dd>
							<input type="submit" value="Update">
							<input type="hidden" name="coriginal" value="<?=$currency->getSymbol()?>"/>
						</dd>
					</dl>
				</form>
			</div>
		</div>
	</body>
</html>