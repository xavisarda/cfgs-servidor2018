<?php 

require_once(__DIR__.'/../controller/IndexController.php');
require_once(__DIR__.'/../inc/Constants.php');

$cnt = new IndexController();

$list = $cnt->getCurrencies();

?><html>
	<head>
		<title>Currencies</title>
	</head>
	<body>
		<div id="wrapper">
			<div id="form">
				<form method="post" action="/forms/create.php">
					<dl>
						<dt><label for="cname">Name</label></dt>
						<dd><input type="text" id="cname" name="cname" /></dd>
						<dt><label for="csymb">Symbol</label></dt>
						<dd><input type="text" id="csymb" name="csymb" /></dd>
						<dt><label for="cev">Euro value</label></dt>
						<dd><input type="numeric" id="cev" name="cev" /></dd>
						<dt><label for="ctype">Type</label></dt>
						<dd>
							<select id="ctype" name="ctype">
								<option value="material">Material</option>
								<option value="crypto">Cryptocurrency</option>
							</select>
						</dd>
						<dd><input type="submit" value="Add"></dd>
					</dl>
				</form>
			</div>
			<hr/>
			<h1>Currencies List</h1>
			<ul>
			<?php foreach($list as $c){ ?>
			<li><strong><?=$c->getName()?></strong></li> 
			<li><?=$c->getSymbol()?></li>
			<li><?=$c->getEurval()?></li>
			<li><?=Constants::$CURRTYPE[$c->getType()]?></li>
			<li></li>
			<?php } ?>
			</ul>
		</div>
	</body>
</html>
