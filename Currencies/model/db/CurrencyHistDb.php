<?php

require_once(__DIR__.'/../CurrencyHist.php');

class CurrencyHistDb{
    
    private $conn;
    
    public function createCurrency($cn, $cs, $cev, $ct, $cvt, $ccid){
        $this->openConnection();
        
        $sql = "INSERT INTO currency_hist ( name, symbol, type, eurval, valtime, cid ) VALUES (?, ?, ?, ?, ?, ?)";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sssssi", $n, $s, $t, $ev, $vt, $cid);
        $n = $cn;
        $s = $cs;
        $t = $ct;
        $ev = $cev;
        $vt = $cvt;
        $cid = $ccid;
        
        $stm->execute();
        $result = $stm->get_result();
        
        return true;
        
    }
    /*
    public function updateCurrency($co, $cn, $cs, $cev, $ct){
        $this->openConnection();
        
        $sql = "UPDATE currency SET name = ? , symbol = ? , type = ? , eurval = ? WHERE symbol = ? ";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sssss", $n, $s, $t, $ev, $o);
        $n = $cn;
        $s = $cs;
        $t = $ct;
        $ev = $cev;
        $o = $co;
        
        $stm->execute();
        $result = $stm->get_result();
        
        return $this->getCurrency($cs);
        
    }
    */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect("127.0.0.1",
                "money",
                "Money!123",
                "money");
        }
    }
    
}