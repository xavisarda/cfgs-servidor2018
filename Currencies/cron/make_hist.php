<?php

require_once(__DIR__.'/../model/db/CurrencyDb.php');
require_once(__DIR__.'/../model/db/CurrencyHistDb.php');

$dbc = new CurrencyDb();
$currencies = $dbc->getCurrencies();

$datetoday = date('Y-m-d');

$dbh = new CurrencyHistDb();
foreach ($currencies as $c){
    $dbh->createCurrency( $c->getName(),
        $c->getSymbol(),
        $c->getEurval(),
        $c->getType(),
        $datetoday,
        $c->getCid());
}


?>