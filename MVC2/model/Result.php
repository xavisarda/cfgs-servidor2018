<?php

require_once(__DIR__.'/Team.php');

class Result{
    
    public $home;
    public $away;
    public $code;
    
    public function __construct($h, $a){
        $this->home = $h;
        $this->away = $a;
    }
    
    public function setCode($c){
        $this->code = $c;
    }
}