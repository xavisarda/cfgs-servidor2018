<?php

require_once(__DIR__.'/Result.php');

class DateResults{
    
    public $num;
    public $results;
    
    public function __construct($n, $r){
        $this->num = $n;
        $this->results = $r;
    }
}