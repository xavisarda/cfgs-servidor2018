<?php 

require_once(__DIR__.'/../Pedido.php');
require_once(__DIR__.'/../../inc/Constants.php');

class PedidoDb{
    
    private $conn;
    
    public function insertPedido($res, $pri, $seg, $pos, $precio, $estado){
        $this->openConnection();
        
        $sql = "INSERT INTO pedido (restaurante, primero, segundo, postre, precio, estado) VALUES (?, ?, ?, ?, ?, ?)";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sssssi", $sres, $spri, $sseg, $spos, $spre, $sestado);
        $sres = $res;
        $spri = $pri;
        $sseg = $seg;
        $spos = $pos;
        $spre = $precio;
        $sestado = $estado;
        
        $stm->execute();
        
    }
    
    
    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }
    
}