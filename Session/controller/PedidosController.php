<?php 

require_once(__DIR__.'/../inc/Constants.php');
require_once(__DIR__.'/../model/db/PedidoDb.php');


class PedidosController{
    
    public function getPedido($id){
        
    }
    
    public function listPedidos(){
        
    }
    
    public function createPedido($res, $pri, $seg, $pos){
        $estado = Constants::$ESTADO_CREADO;
        $precio = $this->getPrecio($pri, $seg, $pos);
        
        $db = new PedidoDb();
        $db->insertPedido($res, $pri, $seg, $pos, $precio, $estado);

    }
    
    public function updatePedido(){
        
    }
    
    public function deletePedido(){
        
    }
    
    private function getPrecio($pri, $seg, $pos){
        $precio = 0;
        
        if($pri != null && $pri != ""){
            $precio += 7;
        }
        
        if($seg != null && $seg != ""){
            $precio += 10;
        }
        
        if($pos != null && $pos != ""){
            $precio += 5;
        }
        
        return $precio;
    }
    
}