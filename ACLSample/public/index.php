<?php
define("RESOURCE", "index");
define("ROLE", isset($_SESSION['user_role'])?$_SESSION['user_role']:'user' );

require_once(__DIR__.'/../lib/controller/WineController.php');

$cnt = new WineController();
$wines = $cnt->indexAction();

?><html>
	<head>
		<title>Wine's Index</title>
	</head>
	<body>
		<div id="wrapper">
			<h1>List of Wines</h1>
			<ul>
			<?php foreach($wines as $w){ ?>
			    <li><?=$w->getName()?>-<?=$w->getYear()?>
			<?php } ?>
			</ul>
		</div>
	</body>
</html>