<?php 

class AppController{
    
    public function isAllowed(){
        $jsonpermissions = file_get_contents(__DIR__.'/../resources/permissions.json');
        $permarray = json_decode($jsonpermissions, true);
        
        if(!in_array(ROLE, $permarray['allowed'][RESOURCE]) 
            || in_array(ROLE, $permarray['banned'][RESOURCE])){
            header("Location: /banned.php");
        }
    }

}