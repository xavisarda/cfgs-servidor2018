<?php 

require_once(__DIR__.'/AppController.php');
require_once(__DIR__.'/../model/db/WineDb.php');

class WineController extends AppController{
    
    public function indexAction(){
        $this->isAllowed();
        
        $db = new WineDb();
        $wines = $db->getWines();
        
        return $wines;
    }
    
    public function detailsAction(){
        
    }
    
    public function updateAction(){
        
    }
    
}