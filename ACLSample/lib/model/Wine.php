<?php

class Wine{
    
    private $_id;
    private $_name;
    private $_year;
    
    public function __construct($name, $year, $id=null){
        $this->setName($name);
        $this->setYear($year);
        $this->setId($id);
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->_year;
    }

    /**
     * @param mixed $_id
     */
    public function setId($_id)
    {
        $this->_id = $_id;
    }

    /**
     * @param mixed $_name
     */
    public function setName($_name)
    {
        $this->_name = $_name;
    }

    /**
     * @param mixed $_year
     */
    public function setYear($_year)
    {
        $this->_year = $_year;
    }

    
    
}