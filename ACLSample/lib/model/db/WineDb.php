<?php 

require_once(__DIR__.'/../Wine.php');

class WineDb{
    
    public function getWines(){
        $db = $this->getDb();
        
        $query = $db->query("SELECT id,name,year FROM wine");
        $wineins = array();
        while ($w = $query->fetchArray()){
            array_push($wineins,new Wine($w[1],$w[2],$w[0]));
        }
        return $wineins;
    }
    
    private function getDb(){
        $db = new SQLite3(__DIR__."/../../resources/Wine.db");
        return $db;
    }
}