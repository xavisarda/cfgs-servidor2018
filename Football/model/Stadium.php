<?php 

class Stadium{
    
    private $_name;
    private $_loc;
    
    public function __construct($n, $loc){
        $this->setName($n);
        $this->setLoc($loc);
    }
    
    public function getName()
    {
        return $this->_name;
    }

    public function getLoc()
    {
        return $this->_loc;
    }

    public function setName($_name)
    {
        $this->_name = $_name;
    }

    /**
     * This param should be given as an array like:
     * [60.5, 75.3]
     * Where the first element is the longitude and the 
     * second one will be the latitude.
     * 
     * @param mixed $_loc
     */
    public function setLoc($_loc)
    {
        $this->_loc = $_loc;
    }

    public function toArray(){
        $arr = array();
        $arr['name'] = $this->getName();
        $arr['loc'] = [ 'long' => $this->getLoc()[0], 
                'lat' => $this->getLoc()[1]];
        return $arr;
    }
    
    public function tojson(){
        $arr = $this->toArray();
        return json_encode($arr);
    }
    
}